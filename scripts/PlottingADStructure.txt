choco install graphviz
Install-Module PSGraph

# PLOT EVERYTHING IN AD:
# $distinguishednames = Get-ADObject -Filter * | Select-Object DistinguishedName

# OR BETTER LIMIT A BIT:
# $distinguishednames = Get-ADObject -Filter * -SearchBase `
# 'cn=users,dc=reskit,dc=org' | Select-Object DistinguishedName

# OR EVEN BETTER:
$distinguishednames = Get-ADObject -LDAPFilter `
 "(|(Objectclass=organizationalunit)(ObjectClass=user))" |
 Select-Object DistinguishedName

$graph = foreach ($element in $distinguishednames) {
  $entry = $element.DistinguishedName.Split(",")
  if ($entry.Length -gt 1) {
    $idx = 0
    do {
      "`"$($entry[$idx])`"->`"$($entry[$idx+1])`""
      $idx++
    } while ($idx -lt ($entry.Length-1))
  } else {
    if ($entry) {$entry}
  }
}

$stringgraph = $graph -join "`n"

Write-Output "strict digraph g { `n $stringgraph `n }" | 
 Export-PSGraph -Destination $env:temp\mysil.pdf -ShowGraph
